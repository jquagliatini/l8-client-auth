export default function getAuthorizationHeader({ username, password }) {
  if (!username || !password) {
    throw new Error('missing username or password');
  }

  return {
    Authorization: `Basic ${btoa(`${username}:${password}`)}`,
  };
}
