export default function fetchAndRetry(url, options, retry = 0) {
  const maxRetry = options.retry || 3;
  return fetch(url, options)
    .then((response) => {
      if (response.status > 499 && retry < maxRetry) {
        return fetchAndRetry(url, options, retry + 1)
      } else {
        return response;
      }
    });
}
