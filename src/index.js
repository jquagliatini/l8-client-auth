import 'bulma/css/bulma.min.css';

import fetchAndRetry from './utils/fetchAndRetry.js';
import getAuthorizationHeader from './utils/getAuthorizationHeader.js';

import Notification from './components/Notification.js';
import PasswordToggler from './components/PasswordToggler.js';

const SERVER = 'http://localhost:1337';

function updateFormHelperText(newInnerHtml, cb) {
  const helperTxt = document.getElementById('changeFormHelperText');
  while (helperTxt.children.length > 0) {
    helperTxt.firstChild.remove();
  }

  helperTxt.innerHTML = newInnerHtml;
  helperTxt.getElementsByTagName('a')[0].addEventListener('click', cb);
}

function toggleFormFields(fields) {
  const form = document.getElementById('form');
  fields.forEach((field) => {
    form[field].classList.toggle('loading');
    form[field].disabled = !form[field].disabled
  });
  form.submit_form.disabled = !form.submit_form.disabled;
}

function submitSigninForm(e) {
  e.preventDefault();

  const FIELDS = ['user_pwd', 'user_email'];
  toggleFormFields(FIELDS);

  fetchAndRetry(
      `${SERVER}/signin`, {
        method: 'POST',
        headers: getAuthorizationHeader({
          username: form.user_email.value,
          password: form.user_pwd.value,
        })
      }
    )
    .then(
      (response) => {
        if (response.status !== 200) {
          throw new Error('Authentication failed, please try again later.');
        }
        return response.json();
      },
      (e) => {
        console.error(e);
        throw new Error('The server seems unreachable, please try again later, or contact us!');
      },
    )
    .then(({
      token
    }) => {
      if (!token) return;
      localStorage.setItem('L8_JWT', token);
      toggleFormFields(FIELDS);
      Notification(
        'Authentication Success!',
        'success',
      );
    })
    .catch((e) => {
      toggleFormFields(FIELDS);
      Notification(
        e.message,
        'danger',
      );
    });
}

function unloadSignupForm() {
  document.title = 'L8 | signin';

  updateFormHelperText(
    'need an account ? <a id="signupLink" href="#/signup">sign up</a>',
    loadSignupForm,
  );

  const form = document.getElementById('form')
  const fields = form.getElementsByClassName('field');

  if (fields.length > 2) {
    fields[2].remove();
  }

  form.removeEventListener('submit', submitSignupForm);
  form.addEventListener('submit', submitSigninForm);
}

function removeDangerInput() {
  const form = document.getElementById('form');
  form.getElementsByClassName('js-help')[0].remove();
  form.user_rep_pwd.classList.toggle('is-danger');
  form.submit_form.disabled = false;

  form.user_rep_pwd.removeEventListener('input', removeDangerInput);
  form.user_pwd.removeEventListener('input', removeDangerInput);
}

function submitSignupForm(e) {
  e.preventDefault();

  const form = document.getElementById('form');

  if (form.user_pwd.value !== form.user_rep_pwd.value) {
    form.user_rep_pwd.classList.toggle('is-danger');
    form.user_rep_pwd.parentElement.insertAdjacentHTML(
      'afterend',
      '<p class="js-help help is-danger" id="repPwdHelp">Wrong password repetition!</p>',
    );
    form.submit_form.disabled = true;

    form.user_rep_pwd.addEventListener('input', removeDangerInput);
    form.user_pwd.addEventListener('input', removeDangerInput);
    return;
  }

  fetchAndRetry(
      `${SERVER}/signup`, {
        method: 'POST',
        headers: getAuthorizationHeader({
          username: form.user_email.value,
          password: form.user_pwd.value,
        }),
      },
    )
    .then((response) => {
      if (response.status === 409) {
        Notification(
          'This email already exists in the application',
          'warning',
        );
      } else if (response.status === 201) {
        Notification(
          'Your account was successfully created, try to log in!',
          'success',
        );
        document.getElementById('signinLink').click();
      } else {
        Notification(
          'An error happened, try again later',
          'danger',
        );
      }
    })
    .catch(() => {
      Notification(
        'The server seems unreachable, try again later or contact us!',
        'warning',
      );
    });
}

function loadSignupForm() {
  document.title = 'L8 | signup';
  updateFormHelperText(
    'already have an account ? <a id="signinLink" href="#/signin">sign in</a>',
    unloadSignupForm,
  );
  const form = document.getElementById('form');
  form.getElementsByClassName('field')[1]
    .insertAdjacentHTML(
      'afterend',
      `<div class="field">
          <div class="control">
            <label for="pwdRepeat" class="label">Repeat Password</label>
            <input
              type="password"
              id="pwdRepeat"
              class="input"
              name="user_rep_pwd"
              required
            >
          </div>
        </div>
      `,
    );
  form.removeEventListener('submit', submitSigninForm);
  form.addEventListener('submit', submitSignupForm);

  form.dispatchEvent(
    new CustomEvent('DOMUpdated', { bubbles: true }),
  );
}

document.addEventListener('DOMContentLoaded', () => {
  if (location.hash.startsWith('#/signup')) {
    loadSignupForm();
  } else if (location.hash.startsWith('#/signin') || location.hash.length === 0) {
    unloadSignupForm();
  }

  let pt = PasswordToggler(
    'input[type="password"]',
  )

  document.addEventListener('DOMUpdated', () => {
    console.log('DOMUpdated');
    pt.uninstall();
    pt = PasswordToggler('input[type="password"]');
  });
});
