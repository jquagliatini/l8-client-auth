function dom(toggle) {
  const checkBox = document.createElement('input');
  checkBox.type = 'checkbox';
  checkBox.id = 'pt-toggler';

  checkBox.addEventListener('change', toggle);

  const label = document.createElement('label');
  label.classList.add('checkbox');
  label.appendChild(checkBox);
  label.appendChild(document.createTextNode(' show password'));

  const p = document.createElement('p');
  p.id = 'js-pt-toggle-container';
  p.classList.add('help');

  p.appendChild(label);

  return p;
}

export default function PasswordToggler(
  elements,
  elementNextToToggle,
) {
  if (!(this instanceof PasswordToggler)) {
    return new PasswordToggler(elements);
  }

  if (typeof elements === 'string') {
    return new PasswordToggler(
      document.querySelectorAll(elements),
      elementNextToToggle,
    );
  }

  this.elements = Array.from(
    document.querySelectorAll('[type="text"][data-pt="true"]')
  ).concat(Array.from(elements));

  elementNextToToggle = elementNextToToggle || (this.elements)[0];

  const self = this;
  this.toggle = () => {
    Array.from(document.querySelectorAll('[data-pt="true"]'))
      .forEach((element) => {
        element.type = element.type === 'password'
          ? 'text'
          : 'password';
        self.currentType = element.type;
      });
  };

  console.log(this.elements);

  elementNextToToggle.insertAdjacentElement('afterend', dom(this.toggle));

  this.elements.forEach((element) => {
    element.dataset['pt'] = 'true';
  });

  if (document.querySelectorAll('[type="text"][data-pt="true"').length > 0) {
    document.getElementById('pt-toggler').checked = true;
    this.elements.forEach((element) => {
      element.type = 'text';
    });
  }
}

PasswordToggler.prototype.uninstall = function () {
  const toggleContainer = document.getElementById('js-pt-toggle-container');
  toggleContainer.remove();
};
