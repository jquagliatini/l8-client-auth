function applyOnInputs(toApply) {
  Array.from(document.querySelectorAll(
    'input[type="email"],input[type="text"],input[type="password"]',
  )).forEach((input) => {
    input[toApply].call(input, 'input', removeNotifications);
  });
}

function removeOnInput() {
  applyOnInputs('addEventListener');
}

function cancelRemoveOnInput() {
  applyOnInputs('removeEventListener');
}

function removeNotifications() {
  Array.from(document.getElementsByClassName('notification'))
    .forEach((notification) => {
      notification.parentNode.remove();
    });
  cancelRemoveOnInput();
}

export default function Notification(text, status = 'success') {
  removeNotifications();
  const html = `
    <div class="columns">
      <div class="column is-one-third"></div>
      <div class="notification is-${status} column is-one-third">
        <button class="delete"></button>
        ${text}
      </div>
    </div>
  `;
  document.getElementsByClassName('section')[0].insertAdjacentHTML(
    'afterbegin',
    html
  );

  document.querySelector('.notification .delete').addEventListener(
    'click',
    () => {
      removeNotifications();
    },
  );
  removeOnInput();
}
