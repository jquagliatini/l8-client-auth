# L8 | client auth

A simple vanilla javascript login portal for [l8-auth-server][u:server].

[u:server]: https://gitlab.com/jquagliatini/l8-server-auth

## Dependencies

- Bulma https://bulma.io

## Running

First of all install every dependency of the project

    $ npm install

To run the web application you first need to run the test server.
To do so, open another terminal and launch the following command:

    $ npm run serve
    started on :1337

Check that it runs by using curl

    $ curl http://localhost:1337/ping
    pong

To start the web server, simply start the node project

    $ npm start

Those commands will trigger the client app server at http://localhost:1234.
Once started and opened in your browser you should see an application
similar to the following

![](capture.png)

Try to register a new user by clicking on the *sign up* link at
the bottom of the form. Once registered you should see the sign in
form instead of the sign up (notice the absent repeat password
field). The fields should be pre-filled with the data you just used
to register the user. When using the "Submit" button, a notification
of success should appear.

At the moment, the application is only a prototype on how to use the
auth REST server. It is not intended for another usage.
Though, every form validation logic used in the application, could
be reused elsewhere. The only hard link at the moment is between the
app and the notification system of bulma.
