module.exports = (credentials) => {
  const [username, password] = Buffer
    .from(
      credentials.replace(/^Basic /, ''),
      'base64',
    )
    .toString('ascii')
    .split(':');
  return { username, password };
}
