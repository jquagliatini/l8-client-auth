module.exports = predicates => (...args) =>
(
  predicates.find(([p]) => p(...args)) ||
  [null, () => {}]
)[1](...args);
