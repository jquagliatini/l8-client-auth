module.exports = state => (p, r, req) => {
  const retryTimes = Number(p.replace(/^\/retry\//, '').replace(/\/.*$/, ''));
  if (!retryTimes || isNaN(retryTimes)) {
    r.writeHead(400);
    r.end('Bad request\n' + retryTimes + ' is not a number!');
  }

  if (state.retries && state.retries.times && state.retries.times === retryTimes) {
    state.retries.inc = state.retries.inc + 1;

    if (state.retries.inc >= state.retries.times) {
      delete state.retries;

      if (/\?.*\bthen\b/.test(req.url)) {
        const [_, matchPath] = /then=([^&]+)/.exec(req.url)
        const redirectPath = matchPath.replace(/\"/g, '');
        r.writeHead(301, {
          Location: 'http://localhost:1234' + redirectPath,
        });
        r.end();
        return;
      }

      r.writeHead(200);
      r.end('ok');
      return;
    }
  } else {
    state.retries = {
      times: retryTimes,
      inc: 1,
    };
  }

  r.writeHead(503);
  r.end(`Service Unavailable\n\nFor ${retryTimes - state.retries.inc}`)
}
