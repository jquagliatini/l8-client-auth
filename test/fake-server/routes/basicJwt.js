const { createHmac } = require('crypto');

const basicAuth = require('../utils/basicAuth.js');

module.exports = (_, r, req) => {
  const { username } = basicAuth(req.headers.authorization);

  const H = createHmac('sha256', 'secret');
  const iat = Math.ceil(Date.now() / 1000);
  const header = Buffer.from(JSON.stringify({
    exp: iat + 3600,
    iat,
  })).toString('base64');
  const payload = Buffer.from(JSON.stringify(
    { email: username },
  )).toString('base64');


  const signature = Buffer.from(
    H.update(header + '.' + payload).digest('hex'),
  ).toString('base64');

  r.writeHead('200', { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
  r.end(JSON.stringify({
    token: header + '.' + payload + '.' + signature,
  }, null, 2));
};
