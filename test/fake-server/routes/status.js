module.exports = function (p, r) {
  const statusCode = Number(p.replace(/^\/status\//, '').replace(/\/.*$/, ''));
  if (!statusCode || isNaN(statusCode)) {
    r.writeHead(400);
    r.end('Bad request\n' + statusCode + ' is not a number!');
    return;
  }

  r.writeHead(statusCode);
  r.end();
};
