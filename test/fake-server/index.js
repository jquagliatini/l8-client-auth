const { createServer } = require('http');
const { parse } = require('url');

const router = require('./router.js');

const state = {};

createServer((req, res) => {
  const url = parse(req.url);
  console.log('[%s] %s', req.method, url.pathname);

  try {
    // res.setHeader('Access-Control-Allow-Origin', '*');
   router(state)(url.pathname, res, req)
  } catch (e) {
    res.writeHead(500);
    res.end(
      'Server Error\n\nMessage: ' +
      e.message +
      '\n\nStack Trace: ' +
      e.stack,
    );
  }
})
  .listen(1337);

console.log('started on :1337');
