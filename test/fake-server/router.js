const retry = require('./routes/retry.js');
const basicJwt = require('./routes/basicJwt.js');
const status = require('./routes/status.js');

const cond = require('./utils/cond.js');
const eq = require('./utils/eq.js');
const startsWith = require('./utils/startsWith.js');

module.exports = state => cond([
  [
    (_, __, req) => eq('OPTIONS')(req.method),
    (_, r) => {
      r.writeHead(204, {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET,POST,HEAD,OPTIONS',
        'Access-Control-Allow-Headers': 'Authorization',
      });
      r.end();
    },
  ],
  [
    eq('/ping'),
    (_, r) => {
      r.writeHead(200, { 'Access-Control-Allow-Origin': '*' });
      r.end('pong');
    },
  ],
  [
    startsWith('/retry'),
    retry(state),
  ],
  [
    (path, _, req) =>
      req.method === 'POST' &&
      path === '/signin' &&
      req.headers.authorization,
    basicJwt,
  ],
  [
    (path, _, req) => eq('/signup')(path) && req.method === 'POST',
    (_, res) => {
      res.writeHead(201, { 'Access-Control-Allow-Origin': '*' });
      res.end();
    },
  ],
  [
    startsWith('/status'),
    status,
  ],
  [
    () => true,
    (_, r) => {
      r.writeHead(404);
      r.end('Not Found');
    },
  ],
]);
